#include <iostream>
using namespace std;

int main(){
	int num1=0;
	int num2=1;
	int count=0;
	cin>>count;
	cout<<num1<<" "<<num2<<" ";
	while(count--){
		int num3 = num1+num2;
		cout<<num3<<" ";
		num1=num2;
		num2=num3;
	}
	cout<<endl;
	return 0;
}
